import Group from '../models/Group';
import Product from '../models/Product';
import PriceListItem from '../models/PriceListItem';

function buildPriceListItem(item) {
  return new PriceListItem(item);
}

function buildProduct(id, name, group) {
  return new Product(id, name, group);
}

function cloneProduct(prevProduct, group, additional) {
  const { priceListItem, currency } = additional;

  const { price, reserved } = prevProduct;

  const product = buildProduct(prevProduct.id, prevProduct.name, group);

  product.price = price;
  product.reserved = reserved;

  product.setCurrency(currency);
  if (priceListItem) {
    product.update(priceListItem);
  }

  return product;
}

function buildGroup(id, name) {
  return new Group(id, name);
}

function cloneGroup(group, additional) {
  const newGroup = buildGroup(group.id, group.name);
  const { priceList, currency } = additional;

  group.products.forEach((currentProduct) => {
    let priceListItem;
    if (priceList[group.id] && priceList[group.id][currentProduct.id]) {
      priceListItem = priceList[group.id][currentProduct.id];
    } else {
      const info = {
        G: currentProduct.id,
        T: additional.group?.id ?? null,
        C: currentProduct.price,
        P: currentProduct.quantity,
      };

      priceListItem = buildPriceListItem(info);
    }

    const additionalInfo = { priceListItem, currency };
    const product = cloneProduct(currentProduct, newGroup, additionalInfo);

    newGroup.pushProduct(product);
  });

  return newGroup;
}

export const modelFactory = {
  group: {
    build: buildGroup,
    clone: cloneGroup,
  },
  product: {
    build: buildProduct,
    clone: cloneProduct,
  },
  priceListItem: {
    build: buildPriceListItem,
  },
};

export default {
  install(Vue) {
    Vue.prototype.$modelFactory = modelFactory;
  },
};
