#!/usr/bin/make
help:
	@echo "runDev		Запускает dev окружение"
	@echo "rundDev		Запускает dev окружение в виде демона"
	@echo "rebuildDev		Запускает dev окружение с ребилдом контейнеров"
	@echo "runProd		Запускает prod окружение"
	@echo "rundProd		Запускает prod окружение в виде демона"
	@echo "rebuildProd		Запускает prod окружение с ребилдом контейнеров"

SHELL = /bin/sh

MY_UID := $(shell id -u)
MY_GID := $(shell id -g)

export MY_UID
export MY_GID

runProd:
	docker-compose -f environment/prod/docker-compose.yaml up
rundProd:
	docker-compose -f environment/prod/docker-compose.yaml up -d
rebuildProd:
	docker-compose -f environment/prod/docker-compose.yaml up --build --force-recreate
runDev:
	docker-compose -f environment/dev/docker-compose.yaml up
rundDev:
	docker-compose -f environment/dev/docker-compose.yaml up -d
rebuildDev:
	docker-compose -f environment/dev/docker-compose.yaml up --build --force-recreate