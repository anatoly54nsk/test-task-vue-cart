import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import services from './service/config';
import apiServicesPlugin from './plugins/apiServicesPlugin';
import modelFactory from './plugins/modelFactory';

Vue.config.productionTip = false;

Vue.use(apiServicesPlugin, { services, client: axios });
Vue.use(modelFactory);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
