class CreateService {
  constructor(url, client, mapper) {
    this.url = url;
    this.client = client;
    this.mapper = mapper;
  }

  get() {
    return this.client.get(this.url).then(this.mapper);
  }
}

export default function createService(url, client, mapper) {
  return new CreateService(url, client, mapper);
}
