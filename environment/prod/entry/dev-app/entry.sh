#!/bin/sh

if [[ "$1" = 'default' ]]; then
  cd /usr/app/projects/app

  yarn

  yarn build
fi
