import createService from '../service/createService';

export default {
  install(Vue, options) {
    const { client, services } = options;
    Vue.prototype.$apiServices = services.reduce((vueServices, service) => {
      const { name, url, mapper } = service;
      vueServices[name] = createService(url, client, mapper);
      return vueServices;
    }, {});

    Vue.prototype.$apiServices.currency = {
      get() {
        const currency = Math.random() * (80 - 20) + 20;
        return Promise.resolve(currency);
      },
    };
  },
};
