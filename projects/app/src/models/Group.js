export default class Group {
  products = [];

  constructor(id, name) {
    this.id = id;
    this.name = name;
  }

  pushProduct(product) {
    this.products.push(product);
  }
}
