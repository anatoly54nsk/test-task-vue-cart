const http = require('http');

const hostname = '0.0.0.0';
const port = 8080;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    const text = [
        'Hi.',
        'This is empty dev server.',
        'For develop any vue project you must place project to projects/app and rerun `make rebuildDev` command.',
    ];
    res.end(text.join('\n'));
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
