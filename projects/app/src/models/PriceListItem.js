export default class PriceListItem {
  constructor(item) {
    const {
      G,
      T,
      C,
      P,
    } = item;

    this.groupId = G;
    this.productId = T;
    this.price = C;
    this.quantity = P;
  }
}
