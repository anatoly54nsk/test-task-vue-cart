import { modelFactory } from '../plugins/modelFactory';

export default [
  {
    name: 'data',
    url: 'http://cart/data/data.json',
    mapper(response) {
      const data = response.data?.Value?.Goods ?? [];
      return data.map((item) => modelFactory.priceListItem.build(item));
    },
  },
  {
    name: 'names',
    url: 'http://cart/data/names.json',
    mapper(response) {
      const data = response.data ?? {};

      return Object.keys(data).map((groupId) => {
        const group = modelFactory.group.build(groupId, data[groupId].G);

        const products = data[groupId].B;
        Object.keys(products).forEach((productId) => {
          const product = modelFactory.product.build(productId, products[productId].N);

          group.pushProduct(product);
        });

        return group;
      });
    },
  },
];
