import Vue from 'vue';
import VueRouter from 'vue-router';
import NotFoundPage from '../views/NotFoundPage.vue';
import CartPage from '../views/CartPage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: CartPage,
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFoundPage,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
