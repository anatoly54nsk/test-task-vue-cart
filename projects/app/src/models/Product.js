export default class Product {
  constructor(id, name, group) {
    this.id = id;
    this.name = name;
    this.group = group;
    this.price = 0; // цена в долларах(USD)
    this.prevPrice = null;
    this.quantity = 0;
    this.reserved = 0;
    this.currency = 1;
  }

  canAdd(quantity) {
    return this.getLeftovers() - quantity >= 0;
  }

  canReserve(quantity) {
    return this.quantity - quantity >= 0;
  }

  isPriceDown() {
    return this.prevPrice ? this.price - this.prevPrice < 0 : false;
  }

  isPriceUp() {
    return this.prevPrice ? this.price - this.prevPrice > 0 : false;
  }

  isValid() {
    return this.price !== 0;
  }

  getLeftovers() {
    return this.quantity - this.reserved;
  }

  getPrice() {
    return this.price * this.currency;
  }

  pushToCart(quantity) {
    if (this.canAdd(quantity)) {
      this.reserved += quantity;
    }
  }

  reserve(quantity) {
    this.reserved = 0;
    if (this.canAdd(quantity)) {
      this.reserved = quantity;
    }
  }

  setCurrency(currency) {
    this.currency = currency;
  }

  update(info) {
    const { price, quantity } = info;
    this.prevPrice = this.price;
    this.price = price;
    this.quantity = quantity;
  }
}
