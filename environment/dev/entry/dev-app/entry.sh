#!/bin/sh

if [[ "$1" = 'default' ]]; then
  if [[ ! -f /usr/app/projects/app/src/main.js ]]; then
    node /usr/app/server/index.js
  else
    cd /usr/app/projects/app
    yarn
    yarn serve
  fi
fi
